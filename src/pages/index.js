import React from "react"
import Navigationbar from './components/navigationbar'
import Topcomponent from './components/topcomponent'
import Middlecomponent from "./components/middlecomponent"
import Twosidecomponent from "./components/twosidecomponent"
import Offerpart from "./components/offerpart"
import Ourprice from "./components/ourprice"
import Calculatecomponent from "./components/calculatecomponent"
import mainStyles from "./components/mainstyles.module.scss"

const IndexPage = () => {
    return (
      <div className={mainStyles.bodyStyle}>      
        <Navigationbar />
        <Topcomponent />
        <Middlecomponent />
        <Twosidecomponent />
        <h2 className={mainStyles.h2}>NASZA OFERTA</h2>
        <div className={mainStyles.offer}>
          <Offerpart name="FOLIA STRETCH BIAŁA" subtitle="transparent 2,5 kg 23my" price="5,99"/>
          <Offerpart name="FOLIA STRETCH CZARNA" subtitle="transparent 2,5 kg 23my" price="6,99"/>
          <Offerpart name="FOLIA STRETCH CZARNA" subtitle="transparent 2,5 kg 23my" price="6,99"/>
        </div>
        <h2 className={mainStyles.h2}>SPRAWDŹ CZY NIE PRZEŁACASZ</h2>
        <h3 className={mainStyles.h3}>Powiedz nam ile zapłacisz za 1 Kg folii a my powiemy Ci ile możesz zaoszczędzić.</h3>
        <Calculatecomponent />
        <Ourprice />
      </div>
    )
}

export default IndexPage