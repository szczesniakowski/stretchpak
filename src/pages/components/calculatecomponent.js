import React from "react"
import calculateStyles from "./calculate.module.scss"

const Calculatecomponent = () => {
    return (
        <div className={calculateStyles.main}>
            <div className={calculateStyles.pick}>
                <div className={calculateStyles.pick_1}> 
                    <div className={calculateStyles.pick_1_image}></div>
                    <p className={calculateStyles.text_p}>FOLIA STRETCH TRANSPARENT</p>
                </div>
                <div className={calculateStyles.pick_2}> 
                    <div className={calculateStyles.pick_2_image}></div>
                    <p className={calculateStyles.text_p}>FOLIA STRETCH CZARNA</p>
                </div>
            </div>
            <div className={calculateStyles.weight}>
                <div className={calculateStyles.weight_1}> 
                    <div className={calculateStyles.weight_checked}></div>
                    <p>WAGA NETTO</p>
                </div>
                <div className={calculateStyles.weight_2}> 
                    <div className={calculateStyles.weight_unchecked}></div>
                    <p>WAGA BRUTTO</p>
                </div>
            </div>
            <div className={calculateStyles.price}>
                <div> 
                    <div></div>
                    <div></div>
                </div>
                <div> 
                    <div></div>
                    <div></div>
                </div>
                <div> 
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    )
}

export default Calculatecomponent