import React from "react"
import ourpriceStyles from "./ourprice.module.scss"

const Ourprice = () => {
    return (
      <div className={ourpriceStyles.main}>
          <h2>NASZA CENA</h2>
          <h1>6,12 / KG</h1>
          <h3>U nas zaoszczędzisz 40 GR na rolce!</h3>
      </div>
    )
}

export default Ourprice