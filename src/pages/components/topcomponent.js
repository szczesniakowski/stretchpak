import React from "react"
import topcomponentStyles from "./topcomponent.module.scss"

const Topcomponent = () => {
    return (
      <div className={topcomponentStyles.main}>
          <div className={topcomponentStyles.left}>
            <h1>NAJWYŻSZEJ JAKOŚCI FOLIA STRETCH</h1>
            <h3>Szukasz wytrzymałej folii do pakowania? Dobrze trafiłeś. Oferujemy najlepszą na polskim rynku folię stretch.</h3>
            <div className={topcomponentStyles.button}>SPRAWDŹ</div>
          </div>
          <div className={topcomponentStyles.right}>

          </div>
      </div>
    )
}

export default Topcomponent