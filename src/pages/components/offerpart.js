import React from "react"
import offerpartStyles from "./offerpart.module.scss"

const Offerpart = (props) => {
    return (
      <div className={offerpartStyles.main}>
          <div className={offerpartStyles.image}><p>SZCZEGÓŁY</p></div>
          <h1 className={offerpartStyles.title}>{props.name}</h1>
          <p className={offerpartStyles.subtitle}>{props.subtitle}</p>
          <div className={offerpartStyles.price}><h2>{props.price}</h2><p> zł netto /rolka</p></div>
      </div>
    )
}

export default Offerpart