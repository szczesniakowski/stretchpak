import React from "react"
import middleStyles from "./middlecomponent.module.scss"

const Middlecomponent = () => {
    return (
    <div className={middleStyles.main}>
    <div className={middleStyles.side}></div>
      <div className={middleStyles.middle}>
        <div className={middleStyles.image}></div>
        <div className={middleStyles.title}>
          Zastosowanie
        </div>
        <div className={middleStyles.text}>
          Folia stretch jest idealnym materiałem do ochrony przedmiotów, które są transportowane jak i przechowywane. Znajdzie ona zastosowanie zarówno w przemyśle, transporcie jak i każdym gospodarstwie domowym. Ze względu na małe grubości i wysoką wytrzymałość gwarantującą stabilność ładunku, folia stretch jest najbardziej ekonomicznym sposobem pakowania produktów Folia wykonana ze stretchu doskonale zabezpieczy chronione elementy przed uszkodzeniami fizycznymi oraz niekorzystnymi warunkami atmosferycznymi.
        </div>
        <div className={middleStyles.button}>SPRAWDŹ OFERTĘ</div>
      </div>
    <div className={middleStyles.side}></div>
    </div>
    )
}

export default Middlecomponent