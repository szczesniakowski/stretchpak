import React from "react"
import twosidecomponentStyles from "./twosidecomponent.module.scss"

const Twosidecomponent = () => {
    return (
      <div className={twosidecomponentStyles.main}>
          <div className={twosidecomponentStyles.left}>

          </div>
          <div className={twosidecomponentStyles.right}>
            <h1>ZALETY NASZEJ FOLII</h1>
            <div className={twosidecomponentStyles.list}>
                <div className={twosidecomponentStyles.listRow}><div className={twosidecomponentStyles.icon_1}></div><p>zapewnia stabilność produktu podczas transportu</p></div>
                <div className={twosidecomponentStyles.listRow}><div className={twosidecomponentStyles.icon_2}></div><p>chroni przed uszkodzeniem</p></div>
                <div className={twosidecomponentStyles.listRow}><div className={twosidecomponentStyles.icon_3}></div><p>chroni przed brudem</p></div>
                <div className={twosidecomponentStyles.listRow}><div className={twosidecomponentStyles.icon_4}></div><p>chroni przed niekorzystnymi warunkami</p></div>
            </div>
          </div>
      </div>
    )
}

export default Twosidecomponent