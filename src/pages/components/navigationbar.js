import React from "react"
import navigationStyles from "./navigationbar.module.scss"

const Navigationbar = () => {
    return (
      <div className={navigationStyles.nav}>
        <div className={navigationStyles.navLogo}>
          <div className={navigationStyles.navItems}>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
          <div  className={navigationStyles.navName}>Stretch Pak</div>
        </div>
        <ul>
          <li className={navigationStyles.navBtn}>ZASTOSOWANIE</li>
          <li className={navigationStyles.navBtn}>ZALETY FOLII</li>
          <li className={navigationStyles.navBtn}>OFERTA</li>
        </ul>
      </div>
    )
}

export default Navigationbar